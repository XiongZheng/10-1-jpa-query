package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findByProductLine(ProductLine productLine);

    List<Product> findAllByQuantityInStockBetween(Short start, Short end);

    List<Product> findAllByQuantityInStockBetweenOrderByProductCode(Short start, Short end);

    List<Product> findTop3ByQuantityInStockGreaterThanAndQuantityInStockLessThanOrderByProductCode(short greater, short less);

    Page<Product> findTop3ByQuantityInStockGreaterThanAndQuantityInStockLessThanOrderByProductCode(short greater, short less, Pageable pageable);

//    List<Product> findByQuantityInStock(@Param("begin") short begin,@Param("end") short end);
    // --end-->
}